﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Rego.Models
{
	public class UserViewModel
	{
		public int UserId { get; set; }
		public string UserName { get; set; }
		public string Password { get; set; }
		public string Email { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string Mobile { get; set; }
		public string Phone { get; set; }
		public string StreetAddress { get; set; }
		public string Suburb { get; set; }
		public Nullable<int> MemberId { get; set; }

		public string Country { get; set; }
		public Nullable<int> TeamId { get; set; }
		public Nullable<int> EmployeeId { get; set; }
		public Nullable<System.Guid> ActivationCode { get; set; }
		public string ChangePasswordCode { get; set; }
		public Nullable<bool> IsEmailVerified { get; set; }

		public virtual EmployeeType EmployeeType { get; set; }
		public virtual MemberType MemberType { get; set; }
		public virtual State State { get; set; }
		public virtual Team Team { get; set; }

		public int StateId { get; set; }
		public string StateName { get; set; }

		public int TeamID { get; set; }
		public string TeamName { get; set; }
		public string TeamExpertName { get; set; }
	}

	public partial class Users
	{
	
		public string ResetPasswordCode { get; set; }
		
	}
}