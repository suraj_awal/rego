﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Rego.Models
{
	[MetadataType(typeof(UserMetadata))]
	public partial class User
	{
		public string ConfirmPassword { get; set; }
	}
	public class UserMetadata
	{
		[Display(Name = "First Name")]
		[Required(AllowEmptyStrings = false, ErrorMessage = "Please Enter Your First Name.")]
		public string FirstName { get; set; }

		[Display(Name = "Last Name")]
		[Required(AllowEmptyStrings = false, ErrorMessage = "Please Enter Your Last Name.")]
		public string LastName { get; set; }

		[Display(Name = "Email ID")]
		[Required(AllowEmptyStrings = false, ErrorMessage = "Please Enter Your Email Address.")]
		[DataType(DataType.EmailAddress)]
		public string Email { get; set; }

		/*[Display(Name = "Date of birth")]
		[DataType(DataType.Date)]
		[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
		public DateTime DateOfBirth { get; set; }*/

		[Required(AllowEmptyStrings = false, ErrorMessage = "Password is required")]
		[DataType(DataType.Password)]
		[MinLength(6, ErrorMessage = "Minimum 6 characters required")]
		public string Password { get; set; }

		/*[Display(Name = "Confirm Password")]
		[DataType(DataType.Password)]
		[Compare("Password", ErrorMessage = "Confirm password and password do not match")]
		public string ConfirmPassword { get; set; }*/

	}
}