﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Rego.Models
{
	public class UserLogin
	{
		[Display(Name = "Email ID")]
		[Required(AllowEmptyStrings = false, ErrorMessage = "Please Enter Your Email Address")]
		public string Email { get; set; }

		[Required(AllowEmptyStrings = false, ErrorMessage = "Please Enter Your Password")]
		[DataType(DataType.Password)]
		public string Password { get; set; }

		[Display(Name = "Remember Me")]
		public bool RememberMe { get; set; }
	}
}