﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Rego.Models;
using System.Web.Security;
using System.Net.Mail;
using System.Web.Helpers;


namespace Rego.Controllers
{
    public class UsersController : Controller
    {
        private RegoDBEntities db = new RegoDBEntities();

        // GET: Users/Create
        public ActionResult Registration()
        {
			ViewBag.State = null;
			using (RegoDBEntities dc = new RegoDBEntities())
			{
				ViewBag.State = new SelectList(dc.States, "StateId", "StateName").ToList();
			}
			//ViewBag.State = new SelectList(db.States, "StateId", "StateName");
			
			return View();
        }
		[HttpPost]
		public JsonResult CheckIFEmailExist(string email)
		{
			using (var context = new RegoDBEntities())
			{
				bool result;
				if(context.CheckEmail(email) == null)
				{
					 result = false;
				}
				else
				{
					result = true; 
				}
			return Json(result);
			}
		}
		// POST: Users/Create
		// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		// more details see https://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Registration([Bind(Include = "UserId,UserName,Password,Email,FirstName,LastName,Mobile,Phone,StreetAddress,Suburb,MemberId,State,Country,TeamId,EmployeeId")] User user)
        {
			bool Status = false;
			string message = "";
			//
			// Model Validation 
			if (ModelState.IsValid)
			{
				#region //Email is already Exist 
				var isExist = IsEmailExist(user.Email);
				if (isExist)
				{
					ModelState.AddModelError("EmailExist", "Email already exists");
					return View(user);
				}
				#endregion

				#region Generate Activation Code 
				user.ActivationCode = Guid.NewGuid();
				#endregion

				#region  Password SHA256ing 
				user.Password = Crypto.SHA256(user.Password);
				/*user.ConfirmPassword = Crypto.SHA256(user.ConfirmPassword);*/ 
				#endregion
				user.IsEmailVerified = false;

				#region Save to Database
				using (RegoDBEntities dc = new RegoDBEntities())
				{	
					//user.StateId = dc.States.Where(a => a.State == user.State).FirstOrDefault();
					dc.Users.Add(user);
					dc.SaveChanges();

					//Send Email to User
					SendVerificationLinkEmail(user.Email, user.ActivationCode.ToString());
					message = "Registered Succesfully.. Account activation link " +
						" has been sent to your email id:" + user.Email;
					Status = true;
				}
				#endregion
			}
			else
			{
				message = "Invalid Request";
			}

			ViewBag.Message = message;
			ViewBag.Status = Status;
			return View(user);
			/*if (ModelState.IsValid)
            {
                db.Users.Add(user);
                db.SaveChanges();
                return RedirectToAction("Index");
            }*/
			
            /*ViewBag.EmployeeId = new SelectList(db.EmployeeTypes, "EmployeeTypeId", "EmployeeType1", user.EmployeeId);
            ViewBag.MemberId = new SelectList(db.MemberTypes, "MemberId", "MemberType1", user.MemberId);
            ViewBag.StateId = new SelectList(db.States, "StateId", "StateName", user.StateId);
            ViewBag.TeamId = new SelectList(db.Teams, "TeamID", "TeamName", user.TeamId);
            return View(user);*/
        }

		public ActionResult VerifyAccount(string id)
		{	
			if(id == null)
			{
				return RedirectToAction("Index");
			}
			else
			{
				bool Status = false;
				using (RegoDBEntities dc = new RegoDBEntities())
				{
					dc.Configuration.ValidateOnSaveEnabled = false; // This line is to avoid 
																	//confirm password doesnt match issue on save changes
					var v = dc.Users.Where(a => a.ActivationCode == new Guid(id)).FirstOrDefault();
					if (v != null)
					{
						v.IsEmailVerified = true;
						dc.SaveChanges();
						Status = true;
					}
					else
					{
						ViewBag.Message = "Invalid Request";
					}
				}

				ViewBag.Status = Status;
				return View();
			}
			
		}

		protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

		[NonAction]
		public bool IsEmailExist(string emailID)
		{
			using (RegoDBEntities dc = new RegoDBEntities())
			{
				var v = dc.Users.Where(a => a.Email == emailID).FirstOrDefault();
				return v != null;
			}
		}

		[NonAction]
		public void SendVerificationLinkEmail(string emailID, string activationCode, string emailFor = "VerifyAccount")
		{
			var verifyUrl = "/Users/" + emailFor + "/" + activationCode;
			var link = Request.Url.AbsoluteUri.Replace(Request.Url.PathAndQuery, verifyUrl);

			var fromEmail = new MailAddress("isameabdo1992@gmail.com", "Ticketing System");
			var toEmail = new MailAddress(emailID);
			var fromEmailPassword = "salwahabibti"; // Replace with actual password

			string subject = "";
			string body = "";
			if (emailFor == "VerifyAccount")
			{
				subject = "Your account is successfully created!";
				body = "<br/><br/>We are excited to tell you that your account is" +
				" successfully created. Please click on the below link to verify your account" +
					" <br/><br/><a href='" + link + "'>" + link + "</a> ";
			}
			else if (emailFor == "ResetPassword")
			{
				subject = "Reset Password";
				body = "Hi,<br/>br/>Please click on the link below to reset your password" +
					"<br/><br/><a href=" + link + ">Reset Password link</a>";
			}

			var smtp = new SmtpClient
			{
				Host = "smtp.gmail.com",
				Port = 587,
				EnableSsl = true,
				DeliveryMethod = SmtpDeliveryMethod.Network,
				UseDefaultCredentials = false,
				Credentials = new NetworkCredential(fromEmail.Address, fromEmailPassword),

			};

			using (var message = new MailMessage(fromEmail, toEmail)
			{
				Subject = subject,
				Body = body,
				IsBodyHtml = true
			})

				smtp.Send(message);
		}


		//Forgot Password

		[HttpGet]
		public ActionResult ForgotPassword()
		{
			return View();
		}

		[HttpPost]
		public ActionResult ForgotPassword(string EmailID)
		{
			//Verify Email ID


			//Generate Reset password link


			//Send Email


			string message = "";
			bool status = false;

			using (RegoDBEntities dc = new RegoDBEntities())
			{
				var account = dc.Users.Where(a => a.Email == EmailID).FirstOrDefault();
				if (account != null)
				{
					//Send email for reset password
					string resetCode = Guid.NewGuid().ToString();
					SendVerificationLinkEmail(account.Email, resetCode, "ResetPassword");
					account.ChangePasswordCode = resetCode;
					//This line I have added here to avoid confirm password not match issue , as we had added a confirm password property 
					//in our model class.
					dc.Configuration.ValidateOnSaveEnabled = false;
					dc.SaveChanges();
					message = "Reset password link has been sent to your email id. Please check your email address.";
				}
				else
				{
					message = "Sorry. Your email address is not valid";
				}
			}
			ViewBag.Message = message;
			return View();
		}

		public ActionResult ResetPassword(string id)
		{
			//Verfiy reset password link
			//find account associated with link
			//Redirect user to reset password page
			using (RegoDBEntities dc = new RegoDBEntities())
			{
				var user = dc.Users.Where(a => a.ChangePasswordCode == id).FirstOrDefault();
				if (user != null)
				{
					ResetPasswordModel model = new ResetPasswordModel();
					model.ResetCode = id;
					return View(model);

				}
				else
				{
					return HttpNotFound();
				}
			}
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult ResetPassword(ResetPasswordModel model)
		{
			var message = "";
			if (ModelState.IsValid)
			{
				using (RegoDBEntities dc = new RegoDBEntities())
				{
					var user = dc.Users.Where(a => a.ChangePasswordCode == model.ResetCode).FirstOrDefault();
					if (user != null)
					{
						user.Password = Crypto.SHA256(model.NewPassword);
						user.ChangePasswordCode = "";
						dc.Configuration.ValidateOnSaveEnabled = false;
						dc.SaveChanges();
						message = "New password updated successfully";
					}
				}
			}
			else
			{
				message = "Something invalid";
			}
			ViewBag.Message = message;
			return View(model);
		}

		//Login
		[HttpGet]
		public ActionResult Login()
		{
			return View();
		}

		//Login POST
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Login(UserLogin login, string ReturnUrl = "")
		{
			string message = "";
			using (RegoDBEntities dc = new RegoDBEntities())
			{
				var v = dc.Users.Where(a => a.Email == login.Email).FirstOrDefault();
				if (v != null)
				{
					bool? a = !v.IsEmailVerified;
					if ((bool)a)
					{
						ViewBag.Message = "Please verify your email first";
						return View();
					}
					if (string.Compare(Crypto.SHA256(login.Password), v.Password) == 0)
					{
						int timeout = login.RememberMe ? 525600 : 20; // 525600 min = 1 year
						var ticket = new FormsAuthenticationTicket(login.Email, login.RememberMe, timeout);
						string encrypted = FormsAuthentication.Encrypt(ticket);
						var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encrypted);
						cookie.Expires = DateTime.Now.AddMinutes(timeout);
						cookie.HttpOnly = true;
						Response.Cookies.Add(cookie);
						var role = dc.MemberTypes.Where(x => x.MemberId == v.MemberId);
						Session["role"] = role;
						if ((string)Session["role"] == "Admin")
						{
							return RedirectToAction("Admin", "User");
						}
						else
						{
							return RedirectToAction("Employee", "User");
						}
						

						//if (Url.IsLocalUrl(ReturnUrl))
						//{
						//	return Redirect(ReturnUrl);
						//}
						//else
						//{
						//	return RedirectToAction("Index", "Home");
						//}
					}
					else
					{
						message = "Invalid credential provided";
					}
				}
				else
				{
					message = "Invalid credential provided";
				}
			}
			ViewBag.Message = message;
			return View();
		}

		public ActionResult Admin()
		{
			ViewBag.Message = "Your Admin page.";

			return View();
		}

		public ActionResult Employee()
		{
			ViewBag.Message = "Your Employee page.";
			return View();

		}
	}
}
